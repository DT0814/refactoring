package com.twu.refactoring;

import java.util.Objects;

public class Direction {
    private final char direction;
    private final char east = 'E', south = 'S', west = 'W', north = 'N';

    public Direction(char direction) {
        this.direction = direction;
    }

    public Direction turnRight() {
        switch (direction) {
            case north:
                return new Direction(east);
            case south:
                return new Direction(west);
            case east:
                return new Direction(north);
            case west:
                return new Direction(south);
            default:
                throw new IllegalArgumentException();
        }
    }

    public Direction turnLeft() {
        switch (direction) {
            case north:
                return new Direction(west);
            case south:
                return new Direction(east);
            case east:
                return new Direction(north);
            case west:
                return new Direction(south);
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Direction)) {
            return false;
        }
        Direction direction1 = (Direction) o;
        return direction == direction1.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(direction);
    }

    @Override
    public String toString() {
        return "Direction{direction=" + direction + '}';
    }
}
